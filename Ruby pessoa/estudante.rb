require_relative "pessoa"
class Student < Person

  @is_studying
  @register

  def initialize cpf = "___.___.___-__", adress = "", name = "", age = "", birth = "", is_studying = false, register = ""
    super cpf, adress, name, age, birth
    @is_studying = is_studying
    @register = register
  end

  def am_i_studying?
    puts @is_studying
  end

  def GetRegister
    return @register
  end

  def ChangeTask
    @is_studying = !@is_studying
  end

end