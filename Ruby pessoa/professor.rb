require_relative "pessoa"
class Professor < Person
  @formation
  @classes
  def initialize cpf = "___.___.___-__", adress = "", name = "", age = "", birth = "", formation = [], classes = []
    super cpf, adress, name, age, birth
    @formation = formation
    @classes = classes
  end
  
  def presentation
    super
  end

  def GetFormation
    return @formation
  end

  def GetClasses
    return @classes
  end

end